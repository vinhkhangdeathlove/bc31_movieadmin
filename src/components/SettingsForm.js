import Card from "@material-tailwind/react/Card";
import CardHeader from "@material-tailwind/react/CardHeader";
import CardBody from "@material-tailwind/react/CardBody";
import Button from "@material-tailwind/react/Button";
import { useSelector } from "react-redux";

export default function SettingsForm() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  const onFinish = () => {};
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <Card>
      <CardHeader color="purple" contentPosition="none">
        <div className="w-full flex items-center justify-between">
          <h2 className="text-white text-2xl">My Account</h2>
          <Button
            color="transparent"
            buttonType="link"
            size="lg"
            style={{ padding: 0 }}
          >
            Profile
          </Button>
        </div>
      </CardHeader>
      <CardBody>
        <div class="mb-3 xl:w-full">
          <label
            for="taiKhoan"
            class="form-label inline-block mb-2 text-gray-700"
          >
            Tài khoản
          </label>
          <input
            type="text"
            disabled={true}
            class="bg-gray-200 form-control w-full px-2 py-1 text-base font-normal text-gray-700 bg-clip-padding border border-solid border-gray-300
        rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
            id="taiKhoan"
            value={userInfor?.taiKhoan}
          />

          <label for="hoTen" class="form-label inline-block mb-2 text-gray-700">
            Họ tên
          </label>
          <input
            type="text"
            class="form-control w-full px-2 py-1 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300
        rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
            id="hoTen"
            value={userInfor?.hoTen}
          />
          <label for="email" class="form-label inline-block mb-2 text-gray-700">
            Email
          </label>
          <input
            type="text"
            class="form-control w-full px-2 py-1 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300
        rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
            id="email"
            value={userInfor?.email}
          />
          <label for="soDT" class="form-label inline-block mb-2 text-gray-700">
            Số điện thoại
          </label>
          <input
            type="text"
            class="form-control w-full px-2 py-1 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300
        rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
            id="soDT"
            value={userInfor?.soDT}
          />
        </div>
        {/* <form>
          <h6 className="text-purple-500 text-sm mt-3 mb-6 font-light uppercase">
            User Information
          </h6>
          <div className="flex flex-wrap mt-10">
            <div className="w-full lg:w-6/12 pr-4 mb-10 font-light">
              <Input type="text" color="purple" placeholder="Username" />
            </div>
            <div className="w-full lg:w-6/12 pl-4 mb-10 font-light">
              <Input type="email" color="purple" placeholder="Email Address" />
            </div>
            <div className="w-full lg:w-6/12 pr-4 mb-10 font-light">
              <Input type="text" color="purple" placeholder="First Name" />
            </div>
            <div className="w-full lg:w-6/12 pl-4 mb-10 font-light">
              <Input type="email" color="purple" placeholder="Last Name" />
            </div>
          </div>

          <h6 className="text-purple-500 text-sm my-6 font-light uppercase">
            Contact Information
          </h6>
          <div className="flex flex-wrap mt-10">
            <div className="w-full lg:w-12/12 mb-10 font-light">
              <Input type="text" color="purple" placeholder="Address" />
            </div>
            <div className="w-full lg:w-4/12 pr-4 mb-10 font-light">
              <Input type="text" color="purple" placeholder="City" />
            </div>
            <div className="w-full lg:w-4/12 px-4 mb-10 font-light">
              <Input type="text" color="purple" placeholder="Country" />
            </div>
            <div className="w-full lg:w-4/12 pl-4 mb-10 font-light">
              <Input type="text" color="purple" placeholder="Postal Code" />
            </div>
          </div>

          <h6 className="text-purple-500 text-sm my-6 font-light uppercase">
            About Me
          </h6>
          <div className="flex flex-wrap mt-10 font-light">
            <Textarea color="purple" placeholder="About Me" />
          </div>
        </form> */}
      </CardBody>
    </Card>
  );
}
