import Card from "@material-tailwind/react/Card";
import CardBody from "@material-tailwind/react/CardBody";
import CardFooter from "@material-tailwind/react/CardFooter";
import Image from "@material-tailwind/react/Image";
import H5 from "@material-tailwind/react/Heading5";
import LeadText from "@material-tailwind/react/LeadText";
import Button from "@material-tailwind/react/Button";
import ProfilePicture from "assets/img/avatar.jpg";
import { useSelector } from "react-redux";

export default function ProfileCard() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  return (
    <Card>
      <div className="flex flex-wrap justify-center">
        <div className="w-48 px-4 -mt-24">
          <Image src={ProfilePicture} rounded raised />
        </div>
      </div>
      <div className="text-center mt-10">
        <H5 color="gray">{userInfor?.hoTen}</H5>
      </div>
      <CardBody>
        <div className="border-t border-lightBlue-200 text-center px-2 ">
          <LeadText color="blueGray">
            An artist of considerable range, Jenna the name taken by
            Melbourne-raised, Brooklyn-based Nick Murphy writes, performs and
            records all of his own music, giving it a warm, intimate feel with a
            solid groove structure. An artist of considerable range.
          </LeadText>
        </div>
      </CardBody>
      <CardFooter>
        <div className="w-full flex justify-center -mt-8">
          <a href="#pablo" className="mt-5" onClick={(e) => e.preventDefault()}>
            <Button color="purple" buttonType="link" ripple="dark">
              Show more
            </Button>
          </a>
        </div>
      </CardFooter>
    </Card>
  );
}
