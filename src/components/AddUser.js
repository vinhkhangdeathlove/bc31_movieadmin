import { Form, Input, message, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { userService } from "../services/user.service";

export default function AddUser() {
  const [typeUser, setTypeUser] = useState([]);
  let history = useNavigate();
  useEffect(() => {
    userService
      .getTypeUser()
      .then((res) => {
        setTypeUser(res.content);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  }, []);

  const onFinish = (values) => {
    values.maNhom = "GP06";
    userService
      .addUser(values)
      .then((res) => {
        message.success("Thêm thành công");
        setTimeout(() => {
          history("/");
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  return (
    <div className="flex justify-center">
      <div className=" w-1/3">
        {" "}
        <Form layout="vertical" onFinish={onFinish} scrollToFirstError>
          {/* Tài khoản */}
          <Form.Item
            type="string"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "vui lòng nhập tài khoản!",
              },
              {
                required: true,
                message: "vui lòng nhập E-mail!",
              },
            ]}
          >
            <Input placeholder="Tài khoản" className="rounded-md" />
          </Form.Item>
          {/* Mật khẩu */}
          <Form.Item
            type="string"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "vui lòng nhập mật khẩu!",
              },
            ]}
          >
            <Input.Password
              placeholder="Mật khẩu"
              className="rounded-md h-10"
            />
          </Form.Item>
          {/* Email */}
          <Form.Item
            type="string"
            name="email"
            rules={[
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
              {
                required: true,
                message: "vui lòng nhập E-mail!",
              },
            ]}
          >
            <Input placeholder="Email" className="rounded-md" />
          </Form.Item>
          {/* Số-ĐT */}
          <Form.Item
            type="string"
            name="soDT"
            rules={[
              {
                required: true,
                message: "vui lòng nhập số điện thoại!",
              },
            ]}
          >
            <Input
              placeholder="Số ĐT"
              style={{
                width: "100%",
              }}
              className="rounded-md"
            />
          </Form.Item>
          {/* Họ-Tên */}
          <Form.Item
            type="string"
            name="hoTen"
            rules={[
              {
                required: true,
                message: "vui lòng nhập họ tên!",
                whitespace: true,
              },
            ]}
          >
            <Input placeholder="Họ tên" className="rounded-md" />
          </Form.Item>
          {/* Loại người dùng */}
          <Form.Item label="Select" name="maLoaiNguoiDung" type="string">
            <Select>
              <Select.Option value={typeUser[0]?.maLoaiNguoiDung}>
                {typeUser[0]?.tenLoai}
              </Select.Option>
              <Select.Option value={typeUser[1]?.maLoaiNguoiDung}>
                {typeUser[1]?.tenLoai}
              </Select.Option>
            </Select>
          </Form.Item>

          <button
            type="submit"
            className="group relative flex w-full justify-center rounded-md border border-transparent bg-[#e5b60d] py-2 px-4 text-sm font-medium text-black hover:text-[#e5b60d] hover:bg-gray-700 hover:duration-500  focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:ring-offset-2"
          >
            <span className="absolute inset-y-0 left-0 flex items-center pl-3"></span>
            Thêm tài khoản
          </button>
        </Form>
      </div>
    </div>
  );
}
