import { Routes, Route } from "react-router-dom";
import Sidebar from "./components/Sidebar";
import Settings from "./pages/Settings";
import TablesUser from "./pages/TablesUser";
import TablesMovies from "./pages/TablesMovies";

import Footer from "./components/Footer";

// Tailwind CSS Style Sheet
import "assets/styles/tailwind.css";
import { LoginPage } from "pages/LoginPage";
import AddUser from "./components/AddUser";

function App() {
  return (
    <>
      <Sidebar />
      <div className="md:ml-64">
        <Routes>
          <Route exact path="/" element={<Settings />} />
          <Route exact path="/listusers" element={<TablesUser />} />
          <Route exact path="/listmovies" element={<TablesMovies />} />
          <Route exact path="/login" element={<LoginPage />} />
          <Route exact path="/adduser" element={<AddUser />} />

          {/* <Redirect from="*" to="/" /> */}
        </Routes>
        <Footer />
      </div>
    </>
  );
}

export default App;
