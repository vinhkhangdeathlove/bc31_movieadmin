import React from "react";
import logoCineMax from "../assets/img/Logo-cinemax.png";
import { LockClosedIcon } from "@heroicons/react/20/solid";
import { Form, Input, message } from "antd";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userService } from "../services/user.service";
import { loginAction } from "../redux/actions/userAction";
import { localStorageServ } from "../services/localStorageService";

export const LoginPage = () => {
  let dispatch = useDispatch();
  let history = useNavigate();

  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postLogin(values)
      .then((res) => {
        console.log("res", res);
        message.success("Đăng nhập thành công");
        dispatch(loginAction(res.content));
        localStorageServ.user.set(res.content);

        setTimeout(() => {
          history("/");
        }, 1000);
      })
      .catch((err) => {
        console.log(err.response.content);
        message.error(err.response.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="bg-login flex min-h-screen items-center justify-center py-12 p-10 sm:px-6 lg:px-8">
      <div className="bg-white max-w-md space-y-8 rounded-lg sm:p-6 lg:p-8 p-10">
        {/* logo */}
        <div>
          <img
            className="mx-auto h-12 w-auto"
            src={logoCineMax}
            alt="logoCineMax"
          />
          <h2 className="mt-6 text-center text-3xl font-bold tracking-tight text-black">
            Đăng nhập tài khoản
          </h2>
          <p className="mt-2 text-center text-sm text-gray-600">
            Hoặc &nbsp;
            <Link
              to="/register"
              className="font-medium text-indigo-600 hover:text-indigo-500"
            >
              Đăng ký tài khoản
            </Link>
          </p>
        </div>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <div className="-space-y-px rounded-md shadow-sm">
            <Form.Item
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tài khoản!",
                },
              ]}
            >
              <Input
                type="name"
                required
                label="Mật khẩu"
                prefix={
                  <UserOutlined className="site-form-item-icon text-xl flex items-center" />
                }
                placeholder="Tài khoản"
                className="mt-4 flex space-x-4 relative w-full appearance-none rounded-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
              />
            </Form.Item>
            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu!",
                },
              ]}
            >
              <Input.Password
                required
                prefix={
                  <LockOutlined className="site-form-item-icon text-xl flex items-center" />
                }
                type="password"
                placeholder="Mật khẩu"
                className="flex space-x-4 my-4 relative w-full appearance-none rounded-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
              />
            </Form.Item>
          </div>
          <Form.Item>
            <div className="flex items-center justify-between">
              <div className="flex items-center">
                <input
                  id="remember-me"
                  name="remember-me"
                  type="checkbox"
                  className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                />
                <label
                  htmlFor="remember-me"
                  className="ml-2 block text-sm text-white"
                >
                  Nhớ mật khẩu
                </label>
              </div>

              <div className="text-sm">
                <a
                  href="#"
                  className="login-form-forgot font-medium text-indigo-600 hover:text-indigo-500"
                >
                  Quên mật khẩu?
                </a>
              </div>
            </div>
          </Form.Item>

          <button
            type="submit"
            className=" mt-4 group relative flex w-full justify-center rounded-md border border-transparent bg-[#ff0000] py-2 px-4 text-sm font-medium text-white hover:bg-gray-700 hover:duration-500  focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:ring-offset-2"
          >
            <span className="absolute inset-y-0 left-0 flex items-center pl-3">
              <LockClosedIcon
                className="h-5 w-5 text-white group-hover:text-black"
                aria-hidden="true"
              />
            </span>
            Đăng nhập
          </button>
        </Form>
      </div>
    </div>
  );
};
