import Card from "@material-tailwind/react/Card";
import CardHeader from "@material-tailwind/react/CardHeader";
import CardBody from "@material-tailwind/react/CardBody";
import { movieService } from "../services/movie.service";
import React, { useEffect, useState } from "react";
import { message } from "antd";

export default function Setting() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    movieService
      .getMovieList()
      .then((res) => {
        let movieList = res.content.map((movie) => {
          return { ...movie };
        });
        setDataMovie(movieList);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let handleMovieDelete = (maPhim) => {
    movieService
      .deleteMovie(maPhim)
      .then((res) => {
        message.success("Xoá thành công");
        console.log(res);
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };

  return (
    <>
      <div className="px-3 md:px-8 h-auto mt-24">
        <div className="container mx-auto max-w-full">
          <div className="grid grid-cols-1 px-4 mb-16">
            <Card>
              <CardHeader color="purple" contentPosition="left">
                <h2 className="text-white text-2xl">Movie Table</h2>
              </CardHeader>
              <CardBody>
                <div className="overflow-x-auto">
                  <table className="items-center w-full bg-transparent border-collapse">
                    <thead>
                      <tr>
                        <th className="table__style text-purple-500">ID</th>
                        <th className="table__style text-purple-500">Image</th>
                        <th className="table__style text-purple-500">Name</th>
                        <th className="table__style text-purple-500">
                          Description
                        </th>
                        <th className="table__style text-purple-500">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {dataMovie.map((movie) => {
                        return (
                          <tr key={movie.maPhim}>
                            <td className="table__style font-light">
                              {movie.maPhim}
                            </td>
                            <td className="table__style font-light">
                              <img
                                src={movie.hinhAnh}
                                className="w-20 h-32"
                                alt=""
                              />
                            </td>
                            <td className="table__style font-light">
                              {movie.tenPhim}
                            </td>
                            <td className="table__style font-light">
                              {movie.moTa.length > 60
                                ? movie.moTa.slice(0, 60) + "..."
                                : movie.moTa}
                            </td>
                            <td className="table__style font-light flex space-x-4 h-40">
                              <button className="text-blue-500">
                                <i className="fa fa-edit" />
                              </button>
                              <button
                                className="text-red-500"
                                onClick={() => {
                                  handleMovieDelete(movie.taiKhoan);
                                  console.log(dataMovie);
                                }}
                              >
                                <i className="fa fa-trash" />
                              </button>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
    </>
  );
}
