import Card from "@material-tailwind/react/Card";
import CardHeader from "@material-tailwind/react/CardHeader";
import CardBody from "@material-tailwind/react/CardBody";
import { userService } from "../services/user.service";
import React, { useEffect, useState } from "react";
import { message } from "antd";
import { NavLink } from "react-router-dom";

export default function Setting() {
  const [dataUser, setDataUser] = useState([]);
  useEffect(() => {
    userService
      .getUserList()
      .then((res) => {
        let userList = res.content.map((user) => {
          return { ...user };
        });
        setDataUser(userList);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let handleUserDelete = (taiKhoan) => {
    userService
      .deleteUser(taiKhoan)
      .then((res) => {
        message.success("Xoá thành công");
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };
  return (
    <>
      <div className="px-3 md:px-8 h-auto mt-24">
        <div className="container mx-auto max-w-full">
          <div className="grid grid-cols-1 px-4 mb-16">
            <Card>
              <CardHeader color="purple" contentPosition="left">
                <h2 className="text-white text-2xl">User Table</h2>
              </CardHeader>
              <CardBody>
                <button className="bg-green-500 rounded-lg px-5 py-2 text-white">
                  <NavLink to={"/adduser"}>Add user</NavLink>
                </button>
                <div className="overflow-x-auto">
                  <table className="items-center w-full bg-transparent border-collapse">
                    <thead>
                      <tr>
                        <th className="table__style text-purple-500">
                          Tài khoản
                        </th>
                        <th className="table__style text-purple-500">Họ tên</th>
                        <th className="table__style text-purple-500">Email</th>
                        <th className="table__style text-purple-500">
                          Số điện thoại
                        </th>
                        <th className="table__style text-purple-500">
                          Loại khách hàng
                        </th>
                        <th className="table__style text-purple-500">
                          Thao tác
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {dataUser.map((user) => {
                        return (
                          <tr>
                            <td className="table__style font-light">
                              {user.taiKhoan}
                            </td>
                            <td className="table__style font-light">
                              {user.hoTen}
                            </td>
                            <td className="table__style font-light">
                              {user.email}
                            </td>
                            <td className="table__style font-light">
                              {user.soDT}
                            </td>
                            <td className="table__style font-light">
                              {user.maLoaiNguoiDung}
                            </td>
                            <td className="table__style font-light flex space-x-4">
                              <button className="text-blue-500">
                                <i className="fa fa-edit" />
                              </button>
                              <button
                                className="text-red-500"
                                onClick={() => {
                                  handleUserDelete(user.taiKhoan);
                                  console.log(dataUser);
                                }}
                              >
                                <i className="fa fa-trash" />
                              </button>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
    </>
  );
}
