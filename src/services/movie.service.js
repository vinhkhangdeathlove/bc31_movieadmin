import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";
import { localStorageServ } from "./localStorageService";

export let movieService = {
  deleteMovie: (maPhim) => {
    return axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`,
      method: "DELETE",
      headers: {
        TokenCybersoft: TOKEN_CYBER,
        ["Authorization"]: "Bearer " + localStorageServ.user.get().accessToken,
      },
    });
  },
  getMovieList: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP06");
  },
};
