import axios from "axios";
import { localStorageServ } from "./localStorageService";
import { https, TOKEN_CYBER } from "./configURL";

export let userService = {
  postLogin: (loginData) => {
    return https.post("api/QuanLyNguoiDung/DangNhap", loginData);
  },
  deleteUser: (taiKhoan) => {
    return axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`,
      method: "DELETE",
      headers: {
        TokenCybersoft: TOKEN_CYBER,
        ["Authorization"]: "Bearer " + localStorageServ.user.get().accessToken,
      },
    });
  },
  addUser: (data) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/ThemNguoiDung",
      method: "POST",
      data: data,
      headers: {
        TokenCybersoft: TOKEN_CYBER,
        ["Authorization"]: "Bearer " + localStorageServ.user.get().accessToken,
      },
    });
  },
  getUserList: () => {
    return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP06");
  },
  getTypeUser: () => {
    return https.get("/api/QuanLyNguoiDung/LayDanhSachLoaiNguoiDung");
  },
};
